#!/usr/local/bin/python
# ysrivastava

import argparse
import getpass
import json
import requests
from requests.auth import HTTPBasicAuth

#############################################################
# SUBROUTINES 
#############################################################

def init_options():
    parser = argparse.ArgumentParser()

    parser.add_argument('-l', '--list-repos', default=True, action="store_true", help="list public bitbucket repos for specified user")
    parser.add_argument('-a', '--all', default=False, action="store_true", help="include private bitbucket repos when listing")
    parser.add_argument('-u', '--user', default="", help="specify user to retrieve repos from")

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--http', default=True, action="store_true", help="print HTTP links for repos")
    group.add_argument('--ssh', default=False, action="store_true", help="print SSH links for repos")

    return parser

def print_repos(opt):
     # --all lists private repos, requiring authentication
    if opt.all:
        pw = getpass.getpass()
        result = requests.get("http://api.bitbucket.org/2.0/repositories/{0}".format(opt.user), auth=(opt.user,pw))
        pw = None
    else:
        result = requests.get("http://api.bitbucket.org/2.0/repositories/{0}".format(opt.user))

    if result.status_code != 200:
        raise Exception('Get /repos/ {0}'.format(result.status_code))
    
    output = json.loads(result.content)

    print ""
    for value in output['values']:
        for link in value['links']['clone']:
             # Only provide the http OR ssh links
            if opt.ssh and link['name'] == "ssh": 
                print link['href']
            elif not opt.ssh and link['name'] == "https":
                print link['href']

#############################################################
# MAIN 
#############################################################

if __name__ == "__main__":

    parser = init_options()
    cli_opt = parser.parse_args()

    while cli_opt.user == "":
        cli_opt.user = raw_input("Username: ")

    if cli_opt.list_repos:
        print_repos(cli_opt)
